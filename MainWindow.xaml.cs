﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;
using System.Drawing;

namespace Interactive_Table_top_Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        readonly static TouchRes tr = new TouchRes(); //Global definition for the custom touch control class.
        readonly static ContainerControl cc = new ContainerControl(); //Global definition for the file container central control class.

        private static int editorInstanceCount = 0; //To keep track the number of editor page being created
        private static int managerInstanceCount = 0; //To keep track the number of file manager page being created

        //Reference 1: https://docs.microsoft.com/en-us/dotnet/framework/wpf/advanced/walkthrough-creating-your-first-touch-application
        //Reference 2: https://docs.microsoft.com/en-us/dotnet/api/system.windows.controls.button?view=netframework-4.8#events
        //Responsible for touch manipulation of every UIElement object
        void Window_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = this;
            e.Handled = true;
        }

        //Utilise UIElement parent class instead.
        void Window_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            //Get the UI object and its RenderTransform matrix
            UIElement objToMove = e.OriginalSource as UIElement;
            Matrix objMatrix = ((MatrixTransform)objToMove.RenderTransform).Matrix;

            //Rotate the UI object
            objMatrix.RotateAt(e.DeltaManipulation.Rotation, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);

            //Scale the UI object
            //The UI object scales uniformly by scaling on x-axis only
            objMatrix.ScaleAt(e.DeltaManipulation.Scale.X, e.DeltaManipulation.Scale.X, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);

            //Move the UI object
            objMatrix.Translate(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);

            //Update the changes of the UI object
            objToMove.RenderTransform = new MatrixTransform(objMatrix);

            Rect containingObj = new Rect(((FrameworkElement)e.ManipulationContainer).RenderSize);
            Rect shapeBounds = objToMove.RenderTransform.TransformBounds(new Rect(objToMove.RenderSize));

            if (e.IsInertial && !containingObj.Contains(shapeBounds))
            {
                e.Complete();
            }

            e.Handled = true;
        }

        void Window_InertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {

            // Decrease the velocity of the Rectangle's movement by 
            // 10 inches per second every second.
            // (10 inches * 96 pixels per inch / 1000ms^2)
            e.TranslationBehavior.DesiredDeceleration = 10.0 * 96.0 / (1000.0 * 1000.0);

            // Decrease the velocity of the Rectangle's resizing by 
            // 0.1 inches per second every second.
            // (0.1 inches * 96 pixels per inch / (1000ms^2)
            e.ExpansionBehavior.DesiredDeceleration = 0.1 * 96 / (1000.0 * 1000.0);

            // Decrease the velocity of the Rectangle's rotation rate by 
            // 2 rotations per second every second.
            // (2 * 360 degrees / (1000ms^2)
            e.RotationBehavior.DesiredDeceleration = 720 / (1000.0 * 1000.0);

            e.Handled = true;
        }
        
        //Closes the application by calling Shutdown()
        private void CloseApp()
        {
            System.Windows.Application.Current.Shutdown();
        }

        /// <summary>
        /// Find the specific child object and set its ZIndex to the highest, appearing on top of other child objects in the canvas
        /// </summary>
        /// <param name="el"></param>
        public void SetChildToTop(UIElement el)
        {
            int startingIndex = 0;

            foreach (UIElement child in mainCanvas.Children)
            {
                if (child.Equals(el))
                {
                    Canvas.SetZIndex(child, startingIndex + 10);
                }
                else
                {
                    Canvas.SetZIndex(child, ++startingIndex);
                }
            }
        }

        public void SetChildManipulationProperty(UIElement el, bool bl)
        {
            foreach (UIElement child in mainCanvas.Children)
            {
                if (child.Equals(el))
                {
                    child.IsManipulationEnabled = bl;
                }
            }
        }

        /// <summary>
        /// Display a error message in a form of message box. Used largely for debugging purposes and has no error logging form.
        /// </summary>
        /// <param name="ex"></param>
        public void DisplayErrorMessage(Exception ex)
        {
            MessageBox.Show("Exception: " + Environment.NewLine + ex.InnerException
                + Environment.NewLine + Environment.NewLine
                + "Message: " + Environment.NewLine + ex.Message, "Source: " + ex.Source, 
                MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ExitButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CloseApp();
            }
        }
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            CloseApp();
        }

        public void OpenPDF(string fileInput)
        {
            //NOT WORKING AT THE MOMENT!!!
            //REFER TO THIS LINK FOR MORE INFO:
            //https://stackoverflow.com/questions/52876813/open-local-html-or-pdf-file-in-wpf-web-view

            // Create PDFContainer object
            PDFContainer pc = new PDFContainer();

            // Create PDF Element
            string fileName = System.IO.Path.GetFileNameWithoutExtension(fileInput);
            fileName = fileName.Replace(" ", "_");
            string fileSource = fileInput;
            Uri fileUri = new Uri(fileSource);

            //Load the PDF file into the container
            //pc.contentWeb.Source = fileUri;
            pc.LoadFile(fileUri);

            //Add viewer to the application's canvas
            mainCanvas.Children.Add(cc.EncloseWeb(pc, fileName));
        }
        public void OpenImage(string fileInput)
        {
            // Create ImageContainer object
            ImageContainer ic = new ImageContainer();

            // Create Image Element
            string imageName = "_" + System.IO.Path.GetFileNameWithoutExtension(fileInput);
            string imageSource = fileInput;
            System.Windows.Controls.Image thatImage = new System.Windows.Controls.Image();
            thatImage.Width = 300;

            // Create source
            BitmapImage thatBitmapImage = new BitmapImage();

            // BitmapImage.UriSource must be in a BeginInit/EndInit block
            thatBitmapImage.BeginInit();
            thatBitmapImage.UriSource = new Uri(imageSource);

            // To save significant application memory, set the DecodePixelWidth or  
            // DecodePixelHeight of the BitmapImage value of the image source to the desired 
            // height or width of the rendered image. If you don't do this, the application will 
            // cache the image as though it were rendered as its normal size rather then just 
            // the size that is displayed.
            // Note: In order to preserve aspect ratio, set DecodePixelWidth
            // or DecodePixelHeight but not both.
            thatBitmapImage.DecodePixelWidth = 1200;
            thatBitmapImage.EndInit();

            // Set image source
            thatImage.Source = thatBitmapImage;
            // Allow the image to translate
            //thatImage.IsManipulationEnabled = true;

            MatrixTransform defaultSpawn = new MatrixTransform();
            defaultSpawn.Matrix = new Matrix(1, 0, 0, 1, 0, 0); //m11, m12, m21, m22, offsetX, offsetY

            thatImage.RenderTransform = defaultSpawn;

            thatImage.HorizontalAlignment = HorizontalAlignment.Stretch;
            thatImage.VerticalAlignment = VerticalAlignment.Stretch;
            ic.AddItem(thatImage);
            ic.AddImage(new ImageBrush(new BitmapImage(new Uri(imageSource, UriKind.Relative))));

            // Add image to the application's canvas
            mainCanvas.Children.Add(cc.EncloseImage(ic, imageName));
        }
        private void FileDialogOpenFile()
        {
            //Reference 1: https://stackoverflow.com/questions/10315188/open-file-dialog-and-select-a-file-using-wpf-controls-and-c-sharp
            //Reference 2: https://docs.microsoft.com/en-us/dotnet/api/microsoft.win32.openfiledialog?view=netframework-4.8
            //Reference 3: https://docs.microsoft.com/en-us/dotnet/framework/wpf/controls/how-to-use-the-image-element
            //This OpenFileDialog component derives from Win32 class instead of System.Windows.Form class

            // Configure open file dialog box
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = ".png";
            openFile.Filter = "Image Files (.png, .jpeg)|*.png;*.jpeg|PDF File (.pdf)|*.pdf|All Files|*.*";

            // Show open file dialog box
            Nullable<bool> result = openFile.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                if (System.IO.Path.GetExtension(openFile.FileName).Equals(".pdf")) //PDF
                {
                    OpenPDF(openFile.FileName);
                }
                else //Image
                {
                    OpenImage(openFile.FileName);
                }

            }
        }
        private void OpenFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                FileDialogOpenFile();
            }
        }
        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            FileDialogOpenFile();
        }

        //public void FileBrowserSpawn()
        //{
        //    FileBrowser fileBrowse = new FileBrowser();

        //    mainCanvas.Children.Add(cc.EncloseFileManager(fileBrowse, "FileManager" + ++managerInstanceCount));
        //}
        public void FileBrowserSpawn(bool md)
        {
            FileBrowser fileBrowse = new FileBrowser(md);

            mainCanvas.Children.Add(cc.EncloseFileManager(fileBrowse, "FileManager" + ++managerInstanceCount));
        }
        private void FileManagerButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                FileBrowserSpawn(false);
            }
        }
        private void FileManagerButton_Click(object sender, RoutedEventArgs e)
        {
            FileBrowserSpawn(false);
        }

        private void CanvasScreenshot()
        {
            //Reference: https://stackoverflow.com/questions/40323022/how-can-i-capture-an-entire-image-from-a-canvas

            //screenShotButton.Visibility = Visibility.Hidden;

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)mainCanvas.ActualWidth, (int)mainCanvas.ActualHeight, 100.0, 100.0, PixelFormats.Default);
            rtb.Render(mainCanvas);

            PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

            //screenShotButton.Visibility = Visibility.Visible;

            try
            {
                String dateString = DateTime.Now.ToString();
                dateString = dateString.Replace("/", "_");
                dateString = dateString.Replace(":", "_");
                dateString = dateString.Replace(" ", "-");
                String newPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + "/" + dateString + ".png";
                //String newPath = "C:/Users/Fitri/Documents/Screenshot_" + dateString + ".png";
                FileStream pngOutput = File.Create(newPath);
                pngEncoder.Save(pngOutput);
                pngOutput.Close();

                MessageBox.Show("The canvas is saved! It is now saved under Desktop", "Screenshot saved!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (IOException)
            {
                MessageBox.Show("The screenshot is unable to be saved due to IOException!", "Screenshot save error!", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }
        private void screenShotButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CanvasScreenshot();
            }
        }
        private void screenShotButton_Click(object sender, RoutedEventArgs e)
        {
            CanvasScreenshot();
        }

        private void OpenWebBrowser()
        {
            PDFContainer webc = new PDFContainer();

            Uri webAddress = new Uri("https://www.google.com/");

            //webc.contentWeb.Source = webAddress;
            webc.LoadFile(webAddress);

            mainCanvas.Children.Add(cc.EncloseWeb(webc, "site"));
        }
        private void webBrowserButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                OpenWebBrowser();
            }
        }
        private void webBrowserButton_Click(object sender, RoutedEventArgs e)
        {
            OpenWebBrowser();
        }

        public void ImportImageToCE(ImageBrush img)
        {
            CanvasEditor canvasEdit = new CanvasEditor();
            canvasEdit.ExternalLoadImage(img);
            //canvasEdit.IsManipulationEnabled = true;
            //canvasEdit.RenderTransform = new MatrixTransform(new Matrix(1, 0, 0, 1, 400, 400)); //Set position for it to pop up in the MainWindow

            mainCanvas.Children.Add(cc.EncloseEditor(canvasEdit, "Editor" + ++editorInstanceCount));
        }
        public void CanvasEditorSpawn()
        {
            CanvasEditor canvasEdit = new CanvasEditor();

            mainCanvas.Children.Add(cc.EncloseEditor(canvasEdit, "Editor" + ++editorInstanceCount));
        }
        private void CallCanvasButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CanvasEditorSpawn();
            }
        }
        private void CallCanvasButton_Click(object sender, RoutedEventArgs e)
        {
            CanvasEditorSpawn();
        }


        //Reference: http://www.codeease.com/posts/wpf-save-canvas-to-xaml-and-load-from-xaml.html
        //UNUSABLE AT THIS STATE
        //private void SaveWorkButton_TouchDown(object sender, TouchEventArgs e)
        //{
        //    if (tr.IsDoubleTap(e, this))
        //    {
        //        try
        //        {
        //            String dateToday = DateTime.Today.ToString("d");
        //            dateToday = dateToday.Replace("/", "_");
        //            String fileName = "C:/Users/Fitri/Documents/TTM_WorkCanvas_" + dateToday + ".xaml";
        //            //String fileName = "C:/Users/Fitri/Documents/testing.xaml";


        //            FileStream fs = File.Open(fileName, FileMode.OpenOrCreate);

        //            //System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
        //            //settings.Indent = true;
        //            //System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(fileName);
        //            //System.Windows.Markup.XamlDesignerSerializationManager manager = new System.Windows.Markup.XamlDesignerSerializationManager(writer);
        //            ////System.Windows.Markup.XamlWriter.Save(((MainWindow)Application.Current.MainWindow), writer);
        //            //System.Windows.Markup.XamlWriter.Save(mainCanvas, writer);

        //            System.Windows.Markup.XamlWriter.Save(mainCanvas, fs);
        //            fs.Close();

        //            MessageBox.Show("Workspace saved as " + fileName);
        //        }
        //        catch (Exception ex)
        //        {
        //            DisplayErrorMessage(ex);
        //            //MessageBox.Show(ex.Message, "Error Exception from SaveWorkButton_TouchDown", MessageBoxButton.OK, MessageBoxImage.Error);
        //            throw;
        //        }
        //    }
        //}

        //private void LoadWorkButton_TouchDown(object sender, TouchEventArgs e)
        //{
        //    if (tr.IsDoubleTap(e, this))
        //    {
        //        try
        //        {
        //            //Refer to OpenFileButton_TouchDown event
        //            // Configure open file dialog box
        //            OpenFileDialog openFile = new OpenFileDialog();
        //            openFile.DefaultExt = ".png";
        //            openFile.Filter = "XAML File (.xaml)|*.xaml;";

        //            // Show open file dialog box
        //            Nullable<bool> result = openFile.ShowDialog();

        //            // Process open file dialog box results
        //            if (result == true)
        //            {
        //                String thatFile = System.IO.Path.GetFullPath(openFile.FileName);

        //                FileStream fs = File.Open(thatFile, FileMode.Open, FileAccess.Read);
        //                //this.Content = System.Windows.Markup.XamlReader.Load(fs) as Canvas;
        //                mainCanvas = System.Windows.Markup.XamlReader.Load(fs) as Canvas;
        //                //mainCanvas.Children.Clear();
        //                //Canvas bufferObj = System.Windows.Markup.XamlReader.Load(fs) as Canvas;
        //                fs.Close();
        //                //foreach (UIElement bufferChild in bufferObj.Children)
        //                //{
        //                //    mainCanvas.Children.Add(bufferChild);
        //                //}
        //            }
        //        }
        //        catch (System.Windows.Markup.XamlParseException xpe)
        //        {

        //        }
        //        catch (Exception ex)
        //        {
        //            DisplayErrorMessage(ex);
        //            throw;
        //        }
        //    }
        //}
    }
}
