﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Interactive_Table_top_Manager
{
    /// <summary>
    /// Interaction logic for CanvasEditor.xaml
    /// </summary>
    public partial class CanvasEditor : Page
    {
        private string destinationSave { get; set; }
        private int textSize;
        private static double defaultInkSize = 5;
        private static double defaultEraserSize = 5;

        private DrawingAttributes inkAtts = new DrawingAttributes();
        private DrawingAttributes highAtts = new DrawingAttributes();

        private bool highlighterMode;
        private bool textMode;

        //Derived from ImageContainer
        private Frame parentFrame;
        private String parentName;
        readonly static TouchRes tr = new TouchRes();
        readonly static ContainerControl cc = new ContainerControl();

        public CanvasEditor()
        {
            InitializeComponent();

            //Default ink and eraser size preset
            InkSizeSlider.Value = defaultInkSize;
            EraserSizeSlider.Value = defaultEraserSize;

            //Default radio preset
            NoneRadio.IsChecked = true;
            BlackColRadio.IsChecked = true;

            destinationSave = null;
            textSize = 11;
            highlighterMode = false;
            textMode = false;

            //MainInkCanvas
            MainInkCanvas.Background = Brushes.White;
            MainInkCanvas.EraserShape = new RectangleStylusShape(defaultEraserSize, defaultEraserSize);
            MainInkCanvas.DefaultDrawingAttributes.FitToCurve = true;

            //inkAtts
            inkAtts.IsHighlighter = false;
            inkAtts.Color = Colors.Black;
            inkAtts.Height = defaultInkSize;
            inkAtts.Width = defaultInkSize;

            //highAtts
            highAtts.IsHighlighter = true;
            highAtts.Color = Colors.Yellow;
            highAtts.Width = defaultInkSize;
            highAtts.Height = defaultInkSize;

            UpdateDrawAttributes(inkAtts);
        }

        public void SetReferenceFrame(Frame frame)
        {
            parentFrame = frame;
        }

        private void UpdateDrawAttributes(DrawingAttributes da)
        {
            MainInkCanvas.DefaultDrawingAttributes = da;
        }

        private void SaveAsImage(String savingDest)
        {
            //Reference: https://stackoverflow.com/questions/8881865/saving-a-wpf-canvas-as-an-image
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)MainInkCanvas.ActualWidth, (int)MainInkCanvas.ActualHeight, 100.0, 100.0, System.Windows.Media.PixelFormats.Default);
            rtb.Render(MainInkCanvas);

            BitmapEncoder bitmapEncode = new PngBitmapEncoder();
            bitmapEncode.Frames.Add(BitmapFrame.Create(new CroppedBitmap(rtb, new Int32Rect(0, 0, (int)MainInkCanvas.ActualWidth, (int)MainInkCanvas.ActualHeight))));

            if (destinationSave == null)
            {
                using (var destination = System.IO.File.OpenWrite(savingDest))
                {
                    bitmapEncode.Save(destination);
                }
            }
            else
            {
                using (var saveNow = System.IO.File.OpenWrite(destinationSave))
                {
                    bitmapEncode.Save(saveNow);
                }
            }
        }

        private String SaveCanvas()
        {
            Microsoft.Win32.SaveFileDialog sfd = new Microsoft.Win32.SaveFileDialog();
            sfd.DefaultExt = ".png";
            sfd.Filter = "PNG Files (*.png)|*.png";
            sfd.Title = "Save work as image";
            sfd.FileName = "Canvas";
            Nullable<bool> result = sfd.ShowDialog();

            if (result == true)
            {
                SaveAsImage(sfd.FileName);
                return sfd.FileName;
            }

            return null;
        }

        public void ExternalLoadImage(ImageBrush img)
        {
            if (img.ImageSource.Width < MainInkCanvas.Width | img.ImageSource.Height < MainInkCanvas.Height)
            {
                MainInkCanvas.Width = img.ImageSource.Width;
                MainInkCanvas.Height = img.ImageSource.Height;
                MainInkCanvas.Background = img;
            }
            else
            {
                img.Stretch = Stretch.Uniform;
                MainInkCanvas.Background = img;
            }
        }
        private void LoadImage()
        {
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.DefaultExt = ".png";
            ofd.Filter = "PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg";
            ofd.Title = "Load image to canvas";
            Nullable<bool> result = ofd.ShowDialog();

            if (result == true)
            {
                ImageBrush picBrush = new ImageBrush(new BitmapImage(new Uri(ofd.FileName, UriKind.Relative)));
                if (picBrush.ImageSource.Width < MainInkCanvas.Width | picBrush.ImageSource.Height < MainInkCanvas.Height)
                {
                    MainInkCanvas.Width = picBrush.ImageSource.Width;
                    MainInkCanvas.Height = picBrush.ImageSource.Height;
                    MainInkCanvas.Background = picBrush;
                }
                else
                {
                    picBrush.Stretch = Stretch.Uniform;
                    MainInkCanvas.Background = picBrush;
                }
            }
        }

        private void addText(MouseButtonEventArgs e, String tt)
        {
            
        }

        //For MainInkCanvas
        private void MainInkCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (textMode == true)
            {
                addText(e, "Sample");
            }
        }

        //Main control events
        private void NewCanvas()
        {
            if (MainInkCanvas.Children.Count > 0 | MainInkCanvas.Strokes.Count > 0 | MainInkCanvas.Background != null)
            {
                MessageBoxResult res = MessageBox.Show("Are you sure to open a new canvas?", "New canvas", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (res == MessageBoxResult.No)
                {
                    return;
                }
            }

            MainInkCanvas.Background = Brushes.White;
            MainInkCanvas.Strokes.Clear();
            MainInkCanvas.Children.Clear();
        }
        private void NewFileButton_Click(object sender, RoutedEventArgs e)
        {
            NewCanvas();
        }

        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            LoadImage();
        }

        private void SaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (destinationSave == null)
            {
                destinationSave = SaveCanvas();
                FileTitleTextBlock.Text = destinationSave;
            }
            else
            {
                SaveAsImage(null);
            }
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            destinationSave = null;
            destinationSave = SaveCanvas();
            FileTitleTextBlock.Text = destinationSave;
        }

        private void ChangeInkSize(double val)
        {
            if (highlighterMode == false)
            {
                inkAtts.Width = val;
                inkAtts.Height = val;
                UpdateDrawAttributes(inkAtts);
            }
            else
            {
                highAtts.Width = val;
                highAtts.Height = val;
                UpdateDrawAttributes(highAtts);
            }
        }
        private void InkSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                ChangeInkSize(e.NewValue);
            }
            catch (NullReferenceException nre)
            {
                ChangeInkSize(defaultInkSize);
                return;
            }
        }

        private void ChangeEraserSize(double val)
        {
            double tempValue = val;
            MainInkCanvas.EraserShape = new RectangleStylusShape(tempValue, tempValue);
        }
        private void EraserSizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                ChangeEraserSize(e.NewValue);
            }
            catch (NullReferenceException nre)
            {
                MainInkCanvas.EraserShape = new EllipseStylusShape(defaultEraserSize, defaultEraserSize);
                throw;
            }
        }

        private void ClearTextButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (TextBlock tb in MainInkCanvas.Children)
            {
                MainInkCanvas.Children.Remove(tb);
            }
        }

        //private void EnableManipulation_Click(object sender, RoutedEventArgs e)
        //{
        //    EnableManipulation.Background = Brushes.Green;
        //    DisableManipulation.Background = Brushes.Red;
        //    cc.ToggleFrameManipulation(parentFrame, true);
        //}

        //private void DisableManipulation_Click(object sender, RoutedEventArgs e)
        //{
        //    EnableManipulation.Background = Brushes.Red;
        //    DisableManipulation.Background = Brushes.Green;
        //    cc.ToggleFrameManipulation(parentFrame, false);
        //}

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            cc.RemoveContainer(parentFrame);
        }

        //Edit Mode Radio
        private void ChangeEditingMode(int val)
        {
            switch (val)
            {
                case 0: //None editing mode
                    ColorPanel.IsEnabled = false;
                    highlighterMode = false;
                    textMode = false;
                    MainInkCanvas.EditingMode = InkCanvasEditingMode.None;
                    break;
                case 1: //Ink editing mode
                    ColorPanel.IsEnabled = true; //true
                    highlighterMode = false;
                    textMode = false;
                    MainInkCanvas.EditingMode = InkCanvasEditingMode.Ink;
                    UpdateDrawAttributes(inkAtts);
                    break;

                case 2: //Highlighter editing mode
                    ColorPanel.IsEnabled = false;
                    highlighterMode = true; //true
                    textMode = false;
                    MainInkCanvas.EditingMode = InkCanvasEditingMode.Ink;
                    UpdateDrawAttributes(highAtts);
                    break;

                case 3: //Eraser by stroke editing mode
                    ColorPanel.IsEnabled = false;
                    highlighterMode = false;
                    textMode = false;
                    MainInkCanvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
                    break;

                case 4: //Eraser by point editing mode
                    ColorPanel.IsEnabled = false;
                    highlighterMode = false;
                    textMode = false;
                    MainInkCanvas.EditingMode = InkCanvasEditingMode.EraseByPoint;
                    break;

                case 5: //Enter text editing mode
                    highlighterMode = false;
                    textMode = true; //true
                    MainInkCanvas.EditingMode = InkCanvasEditingMode.None;
                    break;

                default:
                    break;
            }
        }
        private void NoneRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeEditingMode(0);
        }

        private void InkRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeEditingMode(1);
        }

        private void HighlightRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeEditingMode(2);
        }

        private void EraseByStrokeRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeEditingMode(3);
        }

        private void EraseByPointRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeEditingMode(4);
        }

        private void TextInputRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeEditingMode(5);
        }

        //Color Radio
        private void ChangeInkColor(int val)
        {
            switch (val)
            {
                case 0: //Black
                    inkAtts.Color = Colors.Black;
                    break;

                case 1: //Red
                    inkAtts.Color = Colors.Red;
                    break;

                case 2: //Green
                    inkAtts.Color = Colors.Green;
                    break;

                case 3: //Blue
                    inkAtts.Color = Colors.Blue;
                    break;

                default:
                    break;
            }

            UpdateDrawAttributes(inkAtts);
        }
        private void BlackColRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeInkColor(0);
        }

        private void RedColRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeInkColor(1);
        }

        private void GreenColRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeInkColor(2);
        }

        private void BlueColRadio_Checked(object sender, RoutedEventArgs e)
        {
            ChangeInkColor(3);
        }
        //NOT WORKING AT A MOMENT!
        private void FontSizeBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (textSize != null)
            {
                try
                {
                    textSize = Int32.Parse(FontSizeBox.DisplayMemberPath);
                }
                catch (Exception)
                {
                    return;
                    throw;
                }
            }
            else
            {
                textSize = 11;
            }
        }

        private void NewFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                NewCanvas();
            }
        }

        private void OpenFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                LoadImage();
            }
        }

        private void SaveFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                if (destinationSave == null)
                {
                    destinationSave = SaveCanvas();
                }
                else
                {
                    SaveCanvas();
                }
            }
        }

        private void SaveAsButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                destinationSave = null;
                SaveCanvas();
            }
        }

        private void ClearTextButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                foreach (TextBlock tb in MainInkCanvas.Children)
                {
                    MainInkCanvas.Children.Remove(tb);
                }
            }
        }

        //private void EnableManipulation_TouchDown(object sender, TouchEventArgs e)
        //{
        //    if (tr.IsDoubleTap(e, this))
        //    {
        //        EnableManipulation.Background = Brushes.Green;
        //        DisableManipulation.Background = Brushes.Red;
        //        cc.ToggleFrameManipulation(parentFrame, true);
        //    }
        //}

        //private void DisableManipulation_TouchDown(object sender, TouchEventArgs e)
        //{
        //    if (tr.IsDoubleTap(e, this))
        //    {
        //        EnableManipulation.Background = Brushes.Red;
        //        DisableManipulation.Background = Brushes.Green;
        //        cc.ToggleFrameManipulation(parentFrame, false);
        //    }
        //}

        private void CloseButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                cc.RemoveContainer(parentFrame);
            }
        }

        private void NoneRadio_TouchDown(object sender, TouchEventArgs e)
        {
            NoneRadio.IsChecked = true;
        }

        private void InkRadio_TouchDown(object sender, TouchEventArgs e)
        {
            InkRadio.IsChecked = true;
        }

        private void HighlightRadio_TouchDown(object sender, TouchEventArgs e)
        {
            HighlightRadio.IsChecked = true;
        }

        private void EraseByStrokeRadio_TouchDown(object sender, TouchEventArgs e)
        {
            EraseByStrokeRadio.IsChecked = true;
        }

        private void EraseByPointRadio_TouchDown(object sender, TouchEventArgs e)
        {
            EraseByPointRadio.IsChecked = true;
        }

        private void TextInputRadio_TouchDown(object sender, TouchEventArgs e)
        {
            TextInputRadio.IsChecked = true;
        }

        private void BlackColRadio_TouchDown(object sender, TouchEventArgs e)
        {
            BlackColRadio.IsChecked = true;
        }

        private void RedColRadio_TouchDown(object sender, TouchEventArgs e)
        {
            RedColRadio.IsChecked = true;
        }

        private void GreenColRadio_TouchDown(object sender, TouchEventArgs e)
        {
            GreenColRadio.IsChecked = true;
        }

        private void BlueColRadio_TouchDown(object sender, TouchEventArgs e)
        {
            BlueColRadio.IsChecked = true;
        }

        private void Page_TouchDown(object sender, TouchEventArgs e)
        {
            cc.ModifyZIndex(parentFrame);
        }

        private void ManipulationCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            cc.ToggleFrameManipulation(parentFrame, false);
            //ManipulationCheckBox.Content = "Freeze";
        }

        private void ManipulationCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            cc.ToggleFrameManipulation(parentFrame, true);
            //ManipulationCheckBox.Content = "Unfreeze";
        }

        private void ManipulationCheckBox_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                if (ManipulationCheckBox.IsChecked == true)
                {
                    ManipulationCheckBox.IsChecked = false;
                    return;
                }
                else
                {
                    ManipulationCheckBox.IsChecked = true;
                    return;
                }
            }
        }
    }
}
