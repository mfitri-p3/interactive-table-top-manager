﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Interactive_Table_top_Manager
{
    /// <summary>
    /// Interaction logic for VirtualKeyboard.xaml
    /// </summary>
    public partial class VirtualKeyboard : Page
    {
        public VirtualKeyboard()
        {
            InitializeComponent();
        }

        private Frame parentFrame;
        private String parentName;

        readonly static TouchRes tr = new TouchRes();
        readonly static ContainerControl cc = new ContainerControl();

        public void SetReferenceFrame(Frame refFrame)
        {
            parentFrame = refFrame;
        }
        public void SetReferenceName(String frameName)
        {
            parentName = frameName;
        }

        public ICommand ButtonClickCommand()
        {
            //get { return new DelegateCommand(ButtonClick); }
            return null;
        }


        private void ButtonClick(object param)
        {
            System.Windows.MessageBox.Show("EnterClick!");
        }

        private void closeButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                cc.RemoveContainer(parentFrame);
            }
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            cc.RemoveContainer(parentFrame);
        }

        private void Page_TouchDown(object sender, TouchEventArgs e)
        {
            cc.ModifyZIndex(parentFrame);
        }
    }
}
