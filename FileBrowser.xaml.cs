﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Interactive_Table_top_Manager
{
    //Reference: https://www.codeproject.com/Articles/390514/Playing-with-a-MVVM-Tabbed-TreeView-for-a-File-Exp
    //Reference: https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2010/bb546972(v=vs.100)#creating-a-data-source

    /// <summary>
    /// Interaction logic for FileBrowser.xaml
    /// </summary>
    public partial class FileBrowser : Page
    {
        readonly static FileController fc = new FileController();

        //Derived from ImageContainer
        private Frame parentFrame;
        private String parentName;
        readonly static TouchRes tr = new TouchRes();
        readonly static ContainerControl cc = new ContainerControl();

        private string selectedFilePath;
        private string selectedFileName;

        private bool isCopyMode;
        private bool isCutMode;
        private IList<FileInfo> bufferFiles;

        private DirectoryInfo baseDir;

        public FileBrowser()
        {
            InitializeComponent();

            baseDir = fc.GetBaseDirectory();

            BrowseTreeView.Items.Add(CreateDirectoryTree(baseDir));
        }
        public FileBrowser(bool mode) : this()
        {
            switch (mode)
            {
                case true:
                    MainToolbar.IsEnabled = false;
                    MainToolbar.Visibility = Visibility.Collapsed;
                    break;

                case false:
                    SaveButton.IsEnabled = false;
                    SaveButton.Visibility = Visibility.Collapsed;
                    break;
                
                default:
                    break;
            }
        }

        public void SetReferenceFrame(Frame frame)
        {
            parentFrame = frame;
        }

        //Functions to handle BrowseTreeView and BrowseListView
        public TreeViewItem CreateDirectoryTree(DirectoryInfo givenDir)
        {
            try
            {
                //Reference: https://stackoverflow.com/questions/6239544/populate-treeview-with-file-system-directory-structure
                BrowseTreeView.Items.Clear();
                var directoryItem = new TreeViewItem()
                {
                    Header = givenDir.Name
                };

                foreach (var directory in givenDir.GetDirectories())
                {
                    directoryItem.Items.Add(CreateDirectoryTree(directory));
                }
                /*
                foreach (var file in givenDir.GetFiles())
                {
                    directoryItem.Items.Add(new TreeViewItem(){Header = file.Name});
                }
                */
                return directoryItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "CreateDirectoryTree()", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                throw;
            }
        }
        public DirectoryInfo GetThatDirectory(string name)
        {
            DirectoryInfo tempDir;
            try
            {
                if (baseDir.Name == name)
                {
                    tempDir = baseDir;
                    return tempDir;
                }
                else
                {
                    foreach (var directory in baseDir.GetDirectories("*", SearchOption.AllDirectories))
                    {
                        if (directory.Name == name)
                        {
                            tempDir = directory;
                            return tempDir;
                        }
                    };
                }

                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetThatDirectory()", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                throw;
            }
        }
        public void RefreshListAndTree(string directoryName)
        {
            DirectoryInfo tempDir;
            if (baseDir.Name == directoryName)
            {
                tempDir = baseDir;
                BrowseListView.Items.Clear();
                foreach (var file in tempDir.GetFiles())
                {
                    BrowseListView.Items.Add(file);
                }
                return;
            }
            else
            {
                foreach (var directory in baseDir.GetDirectories("*", SearchOption.AllDirectories))
                {
                    if (directory.Name == directoryName)
                    {
                        tempDir = directory;
                        BrowseListView.Items.Clear();
                        foreach (var file in tempDir.GetFiles())
                        {
                            BrowseListView.Items.Add(file);
                        }
                        return;
                    }
                };
            }
        }
        //Functions to communicate with FileController

        public void CreateFile()
        {
            TreeViewItem tvi = (TreeViewItem)BrowseTreeView.SelectedItem;
            DirectoryInfo tempDir = GetThatDirectory(tvi.Header.ToString());
            fc.CreateThatFile(tempDir.FullName, FileNameTextBox.Text);
            RefreshListAndTree(tempDir.FullName);
        }
        private void NewFileButton_Click(object sender, RoutedEventArgs e)
        {
            CreateFile();
        }
        private void NewFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CreateFile();
            }
        }

        public void OpenFile()
        {
            try
            {
                FileInfo thatFile;
                foreach (var item in BrowseListView.SelectedItems)
                {
                    thatFile = (FileInfo)item;
                    if (thatFile.Extension.Equals(".pdf") | thatFile.Extension.Equals(".PDF"))
                    {
                        ((MainWindow)Application.Current.MainWindow).OpenPDF(thatFile.FullName);
                    }
                    else if (thatFile.Extension.Equals(".png") | thatFile.Extension.Equals(".PNG") | thatFile.Extension.Equals(".jpg") | thatFile.Extension.Equals(".JPG") | thatFile.Extension.Equals(".jpeg"))
                    {
                        ((MainWindow)Application.Current.MainWindow).OpenImage(thatFile.FullName);
                    }
                    else
                    {
                        throw new FileLoadException("The selected files are neither images or PDF. Error thrown under FileLoadException.");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "OpenFile()", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
        }
        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFile();
        }
        private void OpenFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                OpenFile();
            }
        }

        public void DeleteFile()
        {
            FileInfo tempFile;
            foreach (var item in BrowseListView.SelectedItems)
            {
                tempFile = (FileInfo)item;
                fc.RemoveThatFile(tempFile.FullName);
                RefreshListAndTree(tempFile.DirectoryName);
            }
        }
        private void DeleteFileButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteFile();
        }
        private void DeleteFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                DeleteFile();
            }
        }

        public void RenameFile()
        {
            TreeViewItem tvi = (TreeViewItem)BrowseTreeView.SelectedItem;
            DirectoryInfo tempDir = GetThatDirectory(tvi.Header.ToString());
            FileInfo tempFile = (FileInfo)BrowseListView.SelectedItem;
            fc.RenameThatFile(tempDir.FullName, tempFile.Name, FileNameTextBox.Text);
            RefreshListAndTree(tempDir.FullName);
        }
        private void RenameFileButton_Click(object sender, RoutedEventArgs e)
        {
            RenameFile();
        }
        private void RenameFileButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                RenameFile();
            }
        }

        private void CutFile()
        {
            isCutMode = true;
            isCopyMode = false;
            bufferFiles = (IList<FileInfo>)BrowseListView.SelectedItems;
        }
        private void CutButton_Click(object sender, RoutedEventArgs e)
        {
            CutFile();
        }
        private void CutButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CutFile();
            }
        }

        private void CopyFile()
        {
            isCopyMode = true;
            isCutMode = false;
            bufferFiles = (IList<FileInfo>)BrowseListView.SelectedItems;
        }
        private void CopyButton_Click(object sender, RoutedEventArgs e)
        {
            CopyFile();
        }
        private void CopyButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CopyFile();
            }
        }

        private void PasteFile()
        {
            TreeViewItem tvi = (TreeViewItem)BrowseTreeView.SelectedItem;
            DirectoryInfo tempDir = GetThatDirectory(tvi.Header.ToString());
            foreach (var item in bufferFiles)
            {
                if (isCopyMode == true)
                {
                    fc.MoveThatFile(item.FullName, tempDir.FullName, 1); //Copy and Paste
                    RefreshListAndTree(tempDir.FullName);
                }
                else if (isCutMode == true)
                {
                    fc.MoveThatFile(item.FullName, tempDir.FullName, 2); //Cut and Paste
                    RefreshListAndTree(tempDir.FullName);
                }
                else
                {
                    return;
                }
            }

            bufferFiles.Clear();
        }
        private void PasteButton_Click(object sender, RoutedEventArgs e)
        {
            PasteFile();
        }
        private void PasteButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                PasteFile();
            }
        }

        public void CreateFolder()
        {
            TreeViewItem tvi = (TreeViewItem)BrowseTreeView.SelectedItem;
            DirectoryInfo tempDir = GetThatDirectory(tvi.Header.ToString());
            fc.CreateThatFolder(tempDir.FullName, FileNameTextBox.Text);
        }
        private void NewFolderButton_Click(object sender, RoutedEventArgs e)
        {
            CreateFolder();
        }
        private void NewFolderButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CreateFolder();
            }
        }

        public String SaveItem()
        {
            TreeViewItem tvi = (TreeViewItem)BrowseTreeView.SelectedItem;
            DirectoryInfo tempDir = GetThatDirectory(tvi.Header.ToString());
            return tempDir.FullName + "/" + FileNameTextBox.Text;
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveItem();
        }
        private void SaveButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                SaveItem();
            }
        }

        public void CancelBrowser()
        {
            cc.RemoveContainer(parentFrame);
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CancelBrowser();
        }
        private void CancelButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                CancelBrowser();
            }
        }

        private void BrowseTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                if (BrowseTreeView.SelectedItem == null)
                {
                    return;
                }

                TreeViewItem thatItem = (TreeViewItem)BrowseTreeView.SelectedItem;
                string thatName = thatItem.Header.ToString();

                RefreshListAndTree(thatName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "BrowseTreeView_SelectedItemChanged", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void BrowseListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (BrowseListView.SelectedItem != null)
                {
                    FileInfo thatItem = (FileInfo)BrowseListView.SelectedItem;
                    string thatName = thatItem.Name;

                    FileNameTextBox.Text = thatName;
                    selectedFilePath = thatItem.FullName;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "BrowseListView_SelectionChanged", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void FileNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                selectedFileName = FileNameTextBox.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FileNameTextBox_TextChanged", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void Page_TouchDown(object sender, TouchEventArgs e)
        {
            cc.ModifyZIndex(parentFrame);
        }

        //private void ManipulationEnable()
        //{
        //    EnableManipulationButton.Background = System.Windows.Media.Brushes.Green;
        //    DisableManipulationButton.Background = System.Windows.Media.Brushes.Red;
        //    cc.ToggleFrameManipulation(parentFrame, true);
        //}
        //private void EnableManipulationButton_Click(object sender, RoutedEventArgs e)
        //{
        //    ManipulationEnable();
        //}
        //private void EnableManipulationButton_TouchDown(object sender, TouchEventArgs e)
        //{
        //    if (tr.IsDoubleTap(e, this))
        //    {
        //        ManipulationEnable();
        //    }
        //}

        //private void ManipulationDisable()
        //{
        //    EnableManipulationButton.Background = System.Windows.Media.Brushes.Red;
        //    DisableManipulationButton.Background = System.Windows.Media.Brushes.Green;
        //    cc.ToggleFrameManipulation(parentFrame, false);
        //}
        //private void DisableManipulationButton_Click(object sender, RoutedEventArgs e)
        //{
        //    ManipulationDisable();
        //}
        //private void DisableManipulationButton_TouchDown(object sender, TouchEventArgs e)
        //{
        //    if (tr.IsDoubleTap(e, this))
        //    {
        //        ManipulationDisable();
        //    }
        //}

        private void MultiSelectModeCheck_Checked(object sender, RoutedEventArgs e)
        {
            BrowseListView.SelectionMode = SelectionMode.Multiple;
        }
        private void MultiSelectModeCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            BrowseListView.SelectionMode = SelectionMode.Single;
        }
        private void MultiSelectModeCheck_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                if (MultiSelectModeCheck.IsChecked == true)
                {
                    MultiSelectModeCheck.IsChecked = false;
                }
                else
                {
                    MultiSelectModeCheck.IsChecked = true;
                }
            }
        }

        private void ManipulationCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            cc.ToggleFrameManipulation(parentFrame, false);
            //ManipulationCheckBox.Content = "Freeze";
        }

        private void ManipulationCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            cc.ToggleFrameManipulation(parentFrame, true);
            //ManipulationCheckBox.Content = "Unfreeze";
        }

        private void ManipulationCheckBox_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                if (ManipulationCheckBox.IsChecked == true)
                {
                    ManipulationCheckBox.IsChecked = false;
                    return;
                }
                else
                {
                    ManipulationCheckBox.IsChecked = true;
                    return;
                }
            }
        }
    }

    /// <summary>
    /// A controller class for FileBrowser to utilise System.IO
    /// </summary>
    public class FileController
    {
        //Essential variables
        private static string baseDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        private static DirectoryInfo fullDirectory;
        //private DirectoryInfo currentDirectory; //Directory pointer
        //private ObservableCollection<DirectoryInfo> directories;
        //private ObservableCollection<FileInfo> currentFiles;
        //private bool actionResult;

        public FileController()
        {
            fullDirectory = new DirectoryInfo(baseDirectoryPath);
            //currentDirectory = fullDirectory;
            //currentFiles = new ObservableCollection<FileInfo>();
            //directories = new ObservableCollection<DirectoryInfo>();
            //actionResult = false;
        }

        private void ShowErrorMessage(Exception ex, string funcName)
        {
            MessageBox.Show(ex.Message, funcName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        //Refresh the currentFiles based on the newly selected directory
        /*
        private void FileCollectionRefresh()
        {
            foreach (var file in currentDirectory.GetFiles())
            {
                //currentFiles.Add(file);
            }
        }
        */

        public DirectoryInfo GetBaseDirectory()
        {
            return fullDirectory;
        }
        /*
        public DirectoryInfo GetCurrentDirectory()
        {
            return currentDirectory;
        }
        public void SetCurrentDirectory(DirectoryInfo dir)
        {
            currentDirectory = dir;
        }
        public Collection<FileInfo> GetFileList()
        {
            return currentFiles;
        }
        public Collection<DirectoryInfo> GetDirectories()
        {
            return directories;
        }
        public bool GetResult()
        {
            return actionResult;
        }
        public void SetResult(bool state)
        {
            actionResult = state;
        }
        */

        //File tree logic and all
        //Directory as pointer manipulation
        /*
        public void MovePointToAny(string fullPathName)
        {
            
        }
        public void MovePointUp(string fullPathName)
        {
            currentDirectory = currentDirectory.Parent;
        }
        */

        //File Manipulation - New, Open, Delete, Rename, Move
        //Open File
        /*
        public FileInfo GetThatFile(string fileName)
        {
            foreach (var thatFile in currentFiles)
            {
                if (thatFile.Name.Equals(fileName))
                {
                    return thatFile;
                }
            }

            throw new FileNotFoundException("Error! File not found: " + fileName);
        }
        */
        public void CreateThatFile(string pathName, string newFileName)
        {
            try
            {
                string tempName = pathName + "/" + newFileName;
                File.Create(tempName).Dispose();
            }
            catch (IOException ioe)
            {
                ShowErrorMessage(ioe, "CreateThatFile()");
                return;
            }
        }
        public void RemoveThatFile(string fullPathFile)
        {
            try
            {
                FileInfo tempFil = new FileInfo(fullPathFile);
                tempFil.Delete();
            }
            catch (IOException ioe)
            {
                ShowErrorMessage(ioe, "RemoveThatFile()");
                return;
            }
        }
        public void RenameThatFile(string pathName, string oldFileName, string newFileName)
        {
            try
            {
                string tempOldName = pathName + "/" + oldFileName;
                string tempNewName = pathName + "/" + newFileName;
                File.Move(tempOldName, tempNewName);
            }
            catch (IOException ioe)
            {
                ShowErrorMessage(ioe, "RenameThatFile()");
                return;
            }
        }
        public void MoveThatFile(string fullPathFile, string newPath, int index)
        {
            try
            {
                FileInfo oldFile = new FileInfo(fullPathFile);
                DirectoryInfo newDir = new DirectoryInfo(newPath);

                switch (index)
                {
                    case 1: //Copy and Paste
                        File.Copy(oldFile.FullName, newDir + "/" + oldFile.Name);
                        break;
                    case 2: //Cut and Paste
                        File.Copy(oldFile.FullName, newDir + "/" + oldFile.Name);
                        oldFile.Delete();
                        break;

                    default:
                        break;
                }
            }
            catch (IOException ioe)
            {
                ShowErrorMessage(ioe, "MoveThatFile()");
                return;
            }
        }

        //Folder Manipulation - New, Open, Delete, Rename, Move
        /*
        public DirectoryInfo GetThatFolder(string folderName)
        {
            foreach (var thatFolder in directories)
            {
                if (thatFolder.Name.Equals(folderName))
                {
                    return thatFolder;
                }
            }

            throw new DirectoryNotFoundException("Error! Directory not found: " + folderName);
        }
        */
        public void CreateThatFolder(string pathName, string newFolderName)
        {
            try
            {
                //DirectoryInfo newDir = new DirectoryInfo(newFolderName);
                //newDir.CreateSubdirectory(pathName);
                string newFolderPath = pathName + "/" + newFolderName;
                Directory.CreateDirectory(newFolderPath);
            }
            catch (IOException ioe)
            {
                ShowErrorMessage(ioe, "CreateThatFolder()");
                return;
            }
        }
        public void RemoveThatFolder(string fullPathName)
        {
            try
            {
                Directory.Delete(fullPathName);
            }
            catch (IOException ioe)
            {
                ShowErrorMessage(ioe, "RemoveThatFolder()");
                return;
            }
        }
        public void RenameThatFolder(string oldFolderName, string newFolderName)
        {

        }
        public void MoveThatFolder(string folderName, string newPath)
        {

        }
    }
}
