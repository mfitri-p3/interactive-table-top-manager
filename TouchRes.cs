﻿using System;
using System.Collections.Generic;
using System.Diagnostics; //For Stopwatch class
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows; //For Point class
using System.Windows.Input; //For TouchEventArgs class

namespace Interactive_Table_top_Manager
{
    class TouchRes
    {
        //Reference: https://stackoverflow.com/questions/9001023/capturing-double-tap-touch-on-multi-touch-screen
        private readonly Stopwatch doubleTapStopwatch = new Stopwatch();
        private Point lastTapPos;
        private readonly static double SPAN = 0.5; //In seconds

        public TouchRes()
        {

        }
        /// <summary>
        /// Checks whether the users executes a double tap input to an object. Returns true if there is a double tap within fixed timespan.
        /// </summary>
        /// <param name="e">A TouchEventArgs class object.</param>
        /// <param name="w">A Window class object.</param>
        /// <returns></returns>
        public bool IsDoubleTap(TouchEventArgs e, UIElement w)
        {
            Point currentTapPos = e.GetTouchPoint(w).Position;
            bool sameTapPos = ((lastTapPos.X - currentTapPos.X) < 50.0 && (lastTapPos.Y - currentTapPos.Y) < 50.0);
            lastTapPos = currentTapPos;

            TimeSpan elapsed = doubleTapStopwatch.Elapsed;
            doubleTapStopwatch.Restart();
            bool similarTapTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(SPAN));

            return sameTapPos && similarTapTime;
        }
    }
}
