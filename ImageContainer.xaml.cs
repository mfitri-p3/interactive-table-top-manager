﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Interactive_Table_top_Manager
{
    /// <summary>
    /// Interaction logic for ImageContainer.xaml
    /// </summary>
    public partial class ImageContainer : Page
    {
        private Frame parentFrame;
        private String parentName;
        private ImageBrush myImage;

        readonly static TouchRes tr = new TouchRes();
        readonly static ContainerControl cc = new ContainerControl();

        public ImageContainer()
        {
            InitializeComponent();
        }
        public ImageContainer(Frame refFrame) : this()
        {
            SetReferenceFrame(refFrame);
        }
        public ImageContainer(Frame refFrame, String frameName) : this()
        {
            SetReferenceFrame(refFrame);
            SetReferenceName(frameName);
        }

        public void SetReferenceFrame(Frame refFrame)
        {
            parentFrame = refFrame;
        }
        public void SetReferenceName(String frameName)
        {
            parentName = frameName;
        }

        /// <summary>
        /// Add an item into ImageContainer's Grid child
        /// </summary>
        public void AddItem(UIElement uiObj)
        {
            contentGrid.Children.Add(uiObj);
        }
        public void AddImage(ImageBrush img)
        {
            myImage = img;
        }
        private void MoveToCanvasEditor()
        {
            ((MainWindow)Application.Current.MainWindow).ImportImageToCE(myImage);
            cc.RemoveContainer(parentFrame);
        }

        private void closeButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                cc.RemoveContainer(parentFrame);
            }
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            cc.RemoveContainer(parentFrame);
        }

        private void Page_TouchDown(object sender, TouchEventArgs e)
        {
            cc.ModifyZIndex(parentFrame);
        }

        private void OpenCanvasButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                MoveToCanvasEditor();
            }
        }

        private void OpenCanvasButton_Click(object sender, RoutedEventArgs e)
        {
            MoveToCanvasEditor();
        }
    }
}
