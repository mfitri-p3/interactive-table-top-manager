﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Interactive_Table_top_Manager
{
    /// <summary>
    /// Interaction logic for PDFContainer.xaml
    /// </summary>
    public partial class PDFContainer : Page
    {
        private Frame parentFrame;
        private String parentName;

        readonly static TouchRes tr = new TouchRes();
        readonly static ContainerControl cc = new ContainerControl();

        public PDFContainer()
        {
            InitializeComponent();
        }
        public PDFContainer(Frame refFrame) : this()
        {
            SetReferenceFrame(refFrame);
        }
        public PDFContainer(Frame refFrame, String frameName) : this()
        {
            SetReferenceFrame(refFrame);
            SetReferenceName(frameName);
        }

        public void SetReferenceFrame(Frame refFrame)
        {
            parentFrame = refFrame;
        }
        public void SetReferenceName(String frameName)
        {
            parentName = frameName;
        }

        /// <summary>
        /// Add an item into PDFContainer's Grid child
        /// </summary>
        public void LoadFile(Uri webAddress)
        {
            contentWeb.Navigate(webAddress);
        }

        private void closeButton_TouchDown(object sender, TouchEventArgs e)
        {
            if (tr.IsDoubleTap(e, this))
            {
                cc.RemoveContainer(parentFrame);
            }
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            cc.RemoveContainer(parentFrame);
        }

        private void Page_TouchDown(object sender, TouchEventArgs e)
        {
            cc.ModifyZIndex(parentFrame);
        }
    }
}
