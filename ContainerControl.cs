﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Interactive_Table_top_Manager
{
    class ContainerControl
    {
        private static List<Frame> listOfFrames = new List<Frame>();

        public ContainerControl()
        {

        }

        private void AddList(Frame fr)
        {
            listOfFrames.Add(fr);
        }
        private void RemoveList(Frame fr)
        {
            listOfFrames.Remove(fr);
        }

        //For access to MainWindow. This current approach does not conform to MVVM (Model-View-ViewModel) design however.
        //Start

        /// <summary>
        /// Closes the given container along with its children.
        /// </summary>
        /// <param name="frame"></param>
        public void RemoveContainer(Frame frame)
        {
            //Reference: https://stackoverflow.com/questions/17001486/how-to-access-wpf-mainwindow-controls-from-my-own-cs-file
            ((MainWindow)Application.Current.MainWindow).mainCanvas.Children.Remove(frame);
            RemoveList(frame);
        }

        /// <summary>
        /// Modifies the ZIndex of every UIElement child found in MainWindow.mainCanvas. Any container invoking this method will be set to the top of the ZIndex.
        /// </summary>
        /// <param name="el_con"></param>
        public void ModifyZIndex(UIElement el_con)
        {
            ((MainWindow)Application.Current.MainWindow).SetChildToTop(el_con);
        }

        /// <summary>
        /// Toggles the IsManipulationEnabled property of a given container found in MainWindow.mainCanvas.
        /// </summary>
        /// <param name="pf"></param>
        /// <param name="bl"></param>
        public void ToggleFrameManipulation(Frame referenceFrame, bool bl)
        {
            ((MainWindow)Application.Current.MainWindow).SetChildManipulationProperty(referenceFrame, bl);
        }

        //End

        private MatrixTransform GenerateMatrixTransSpawn()
        {
            MatrixTransform mt = new MatrixTransform(new Matrix(1, 0, 0, 1, 300, 150));
            return mt;
        }

        private Frame GenerateDefaultFrame(Page pg)
        {
            Frame frame = new Frame();
            frame.Width = pg.Width;
            frame.Height = pg.Height;
            frame.BorderThickness = new Thickness(1);
            frame.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0)); //Black color
            frame.IsManipulationEnabled = true;
            //frame.RenderTransformOrigin = new Point(0.5, 0.5);
            frame.RenderTransform = GenerateMatrixTransSpawn();

            return frame;
        }

        /// <summary>
        /// Returns the ImageContainer object under a Frame class object
        /// </summary>
        /// <param name="pg"></param>
        /// <param name="frameName"></param>
        /// <returns></returns>
        public Frame EncloseImage(ImageContainer pg, string frameName)
        {
            Frame frame = new Frame();
            //frame.Name = frameName;
            frame.Width = pg.Width;
            frame.Height = pg.Height;
            frame.BorderThickness = new Thickness(1);
            frame.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0)); //Black color
            frame.IsManipulationEnabled = true;
            frame.RenderTransform = GenerateMatrixTransSpawn();
            //frame.Name.Replace(frame.Name, frameName);

            pg.SetReferenceFrame(frame);
            //pg.SetReferenceName(frame.Name);

            frame.Navigate(pg);

            AddList(frame);

            return frame;
        }

        /// <summary>
        /// Returns the PDFContainer object under a Frame class object
        /// </summary>
        /// <param name="pg"></param>
        /// <param name="frameName"></param>
        /// <returns></returns>
        public Frame EncloseWeb(PDFContainer pg, string frameName)
        {
            Frame frame = new Frame();
            //frame.Name = frameName;
            frame.Width = pg.Width;
            frame.Height = pg.Height;
            frame.BorderThickness = new Thickness(1);
            frame.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 0, 0)); //Black color
            frame.IsManipulationEnabled = true;
            frame.RenderTransform = GenerateMatrixTransSpawn();

            pg.contentWeb.Height = 650;
            pg.contentWeb.Width = 500;

            pg.SetReferenceFrame(frame);
            //pg.SetReferenceName(frame.Name);

            frame.Navigate(pg);

            AddList(frame);

            return frame;
        }

        public Frame EncloseEditor(CanvasEditor ce, string frameName)
        {
            ce.Height = 600;
            ce.Width = 500;

            Frame frame = GenerateDefaultFrame(ce);
            frame.Name = frameName;
            //frame.IsManipulationEnabled = true;

            ce.SetReferenceFrame(frame);

            frame.Navigate(ce);

            AddList(frame);

            return frame;
        }

        public Frame EncloseFileManager(FileBrowser fb, string frameName)
        {
            fb.Height = 400;
            fb.Width = 750;

            Frame frame = GenerateDefaultFrame(fb);
            frame.Name = frameName;
            //frame.IsManipulationEnabled = true;

            fb.SetReferenceFrame(frame);

            frame.Navigate(fb);

            AddList(frame);

            return frame;
        }

        
    }
}
